# Fancy HTTP

#### The new declarative way to create http clients.

### How to install?

```shell
$ composer require waffler/rest
```

* This package requires PHP 8 or above.

### How to test?

```shell
$ composer test
```

## Quick start

For our example, lets imagine that we want to consume an ordinary API: `https://foo-bar.baz/api`

Our objectives are:

- Perform the login to retrieve the authorization token.
- Retrieve all posts from the database.

#### Step 1: Create the basic interface for your client.

```php
<?php // FooClient.php

namespace App\Clients;

interface FooClient
{
    /**
     * Performs the login int the API.
     * 
     * @param array $credentials Just pass the login and password.
     * @return array             The json response.
     */
    public function login(array $credentials): array;
    
    /**
     * Retrieves all posts.
     * 
     * @param string $authToken The authorization token.
     * @param array $query      Some optional query string Filters.
     * @return array            The list of posts.
     */
    public function getPosts(string $authToken, array $query = []): array: 
}
```

#### Step 2: Annotate the methods with Waffler Attributes.

The magic is almost done. Now we need to annotate the methods and parameters to "teach" Waffler how to make the
requests. There are dozens of Attributes, but for this example we just need 5 of them.

Import the Attributes from the `Waffler\Definition\Attributes` namespace.

```php
<?php // FooClient.php

namespace App\Clients;

use Waffler\Definitions\Attributes\{
    Get,
    Post,
    Query,
    Json,
    Auth\Bearer
};

interface FooClient
{
    /**
     * Performs the login int the API.
     * 
     * @param array $credentials Just pass the login and password.
     * @return array             The json response.
     */
    #[Post('/auth/login')]
    public function login(#[Json] array $credentials): array;
    
    /**
     * Retrieves all posts.
     * 
     * @param string $authToken The authorization token.
     * @param array $query      Some optional query string Filters.
     * @return array            The list of posts.
     */
    #[Get('/posts')]
    public function getPosts(#[Bearer] string $authToken, #[Query] array $query = []): array: 
}
```

#### Step 3: Generate the implementation for your interface and use it.

Import the class `Waffler\Rest\Client` and call the static method `implements` passing the
*fully qualified name* of the interface we just created as first argument and an associative array of GuzzleHttp client
options as second argument.

```php
<?php // PostsController.php

namespace App\Controllers;

use Waffler\Rest\Client;
use App\Clients\FooClient;

class PostsController
{
    private FooClient $fooClient;

    public function __constructor() 
    {
        $this->fooClient = Client::implements(FooClient::class, ['base_uri' => 'https://foo-bar.baz/api']);
    }

    /**
     * Lists all posts.
     *  
     * @return array
     */
    public function index()
    {
        $credentials = $this->fooClient->login([
            'email' => 'email@test.com',
            'password' => 'secret'
        ]);
        
        $posts = $this->fooClient->getPosts($credentials['token'], ['created_at' => '2020-01-01'])
        
        return response()->json($posts)
    }
```

#### Step 4: Read the docs to discover about the other attributes.

** Work in progress **