<?php

namespace Tests\Clients;

/**
 * Trait TestCaseTrait.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests\Clients
 * @mixin \Tests\Clients\TestCaseClient
 */
trait TraitForTesting
{
    public bool $initialized = false;

    public function traitMethod(): int
    {
        return $this->get();
    }

    private function bootTraitForTesting(): void
    {
        $this->initialized = true;
    }
}