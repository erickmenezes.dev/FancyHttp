<?php


namespace Tests\Clients;


use Waffler\Definitions\Attributes\AutoMapped;
use Waffler\Definitions\Attributes\MapTo;

#[AutoMapped]
interface BarInterface
{
    #[MapTo('bar')]
    public function getBar(): string;
}