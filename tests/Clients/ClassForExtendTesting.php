<?php

namespace Tests\Clients;

/**
 * Class ClassForExtendTesting.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests\Clients
 */
abstract class ClassForExtendTesting implements TestCaseClient
{
    public function testExtending(): int
    {
        return $this->get();
    }
}