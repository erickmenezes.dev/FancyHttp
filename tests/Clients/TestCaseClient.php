<?php

namespace Tests\Clients;

use ArrayObject;
use Waffler\Definitions\Attributes\Auth\Basic;
use Waffler\Definitions\Attributes\Auth\Bearer;
use Waffler\Definitions\Attributes\Auth\Digest;
use Waffler\Definitions\Attributes\Auth\Ntml;
use Waffler\Definitions\Attributes\Body;
use Waffler\Definitions\Attributes\Delete;
use Waffler\Definitions\Attributes\FormParamItem;
use Waffler\Definitions\Attributes\FormParams;
use Waffler\Definitions\Attributes\Get;
use Waffler\Definitions\Attributes\Head;
use Waffler\Definitions\Attributes\HeaderParam;
use Waffler\Definitions\Attributes\Headers;
use Waffler\Definitions\Attributes\Json;
use Waffler\Definitions\Attributes\JsonParam;
use Waffler\Definitions\Attributes\Multipart;
use Waffler\Definitions\Attributes\Patch;
use Waffler\Definitions\Attributes\Path;
use Waffler\Definitions\Attributes\PathParam;
use Waffler\Definitions\Attributes\Post;
use Waffler\Definitions\Attributes\Put;
use Waffler\Definitions\Attributes\Query;
use Waffler\Definitions\Attributes\QueryParam;
use Waffler\Definitions\Attributes\RawOptions;
use Waffler\Definitions\Attributes\ReturnsMappedList;
use Waffler\Definitions\Attributes\Suppress;
use Waffler\Definitions\Attributes\Unwrap;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\ResponseInterface;
use Waffler\Rest\Lib\Attributes\ExtendsClass;
use Waffler\Rest\Lib\Attributes\UseTraits;

/**
 * Interface TestCaseClient
 *
 * @property-read ResponseInterface|null $lastResponse
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests\Clients
 * @mixin \Tests\Clients\TraitForTesting
 */
#[UseTraits([TraitForTesting::class])]
#[ExtendsClass(ClassForExtendTesting::class)]
#[Path('/first')]
interface TestCaseClient
{
    // verbs test

    #[Get('/')]
    public function get(): int;

    #[Post('/')]
    public function post(): int;

    #[Put('/')]
    public function put(): int;

    #[Patch('/')]
    public function patch(): int;

    #[Delete('/')]
    public function delete(): int;

    #[Head('/')]
    public function head(): int;

    // headers test

    #[Get('/')]
    public function headersAttribute(#[Headers] array $headers): int;

    #[Get('/')]
    public function headerParamAttribute(#[HeaderParam('X-Foo')] string $value = null): int;

    #[Get('/')]
    public function bearer(#[Bearer] string $token = 't'): int;

    #[Get('/')]
    public function basic(#[Basic] array $auth): int;

    #[Get('/')]
    public function digest(#[Digest] array $auth): int;

    #[Get('/')]
    public function ntml(#[Ntml] array $auth): int;

    // sending data test

    #[Post('/')]
    public function body(#[Body] string $body): int;

    #[Post('/')]
    public function json(#[Json] array $json): int;

    #[Get('/')]
    public function query(#[Query] array $query): int;

    #[Get('/')]
    public function queryParams(#[QueryParam('foo')] string $foo, #[QueryParam('bar')] string $bar): int;

    #[Get('/')]
    public function jsonParams(#[JsonParam('foo')] int $foo, #[JsonParam('bar')] int $bar): int;

    #[Get('/{foo}/{bar}')]
    public function pathParams(#[PathParam('foo')] string $foo, #[PathParam('bar')] string $bar): int;

    #[Get('/')]
    public function multipart(#[Multipart] array $multipart): int;

    #[Get('/')]
    public function formParams(#[FormParams] array $formParams): int;

    #[Get('/')]
    public function formParamItem(#[FormParamItem('foo')] string $foo, #[FormParamItem('bar')] string $bar): int;

    #[Get('/')]
    public function rawOptions(#[RawOptions] array $options): int;

    // casting response test

    #[Get('/')]
    public function castToArray(): array;

    #[Get('/')]
    public function castToObject(): object;

    #[Get('/')]
    public function castToBool(): bool;

    #[Get('/')]
    public function castToString(): string;

    #[Get('/')]
    public function castToInt(): int;

    #[Get('/')]
    public function castToArrayObject(): ArrayObject;

    #[Get('/')]
    public function castToResponse(): ResponseInterface;

    #[Get('/')]
    public function castToVoid(): void;

    #[Get('/')]
    public function castToMixed(): mixed;

    #[Get('/')]
    public function castToDefault();

    #[Get('/')]
    public function castToPromise(): PromiseInterface;

    #[Get('/')]
    public function castToCastable(): CastableForTesting;

    #[Get('/')]
    public function castToAutoMapped(): FooInterface;

    #[Get('/')]
    #[ReturnsMappedList(FooInterface::class)]
    public function castToAutoMappedList(): array;

    #[Get('/')]
    #[ReturnsMappedList(FooInterface::class)]
    public function castToAutoMappedListCustomTraversable(): TraversableForTesting;

    #[Get('/')]
    #[ReturnsMappedList(FooInterface::class)]
    public function castToAutoMappedListBuiltinTraversable(): \ArrayObject;

    // Instruction attributes test

    #[Get('/')]
    #[Unwrap]
    public function unwrap(): array;

    #[Get('/')]
    #[Suppress]
    public function suppress(): int;

    #[Path('second')]
    #[Get('/third')]
    public function path(): int;
}
