<?php

namespace Waffler\Rest\Traits;

/**
 * Trait MethodCallsForwarder.
 *
 * Common shared methods to forward the call from the generated client implementation, to the call handler.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Rest\Traits
 * @internal
 */
trait ImplementationHelper
{
    public function __construct(protected object $handler)
    {
        $this->_initWafflerIntegratedTraits();
    }

    public function __get(string $name): mixed
    {
        return $this->handler->$name;
    }

    public function __set(string $name, mixed $value): void
    {
        $this->handler->$name = $value;
    }

    private function _callHandler(string $method, array $arguments): mixed
    {
        $returnValue = $this->handler->{$method}(...$arguments);

        return $returnValue === $this->handler ? $this : $returnValue;
    }

    private function _initWafflerIntegratedTraits(): void
    {
        foreach (class_uses($this::class) as $trait) {
            $method = 'boot' . (new \ReflectionClass($trait))->getShortName();

            if (method_exists($this, $method)) {
                $this->$method();
            }
        }
    }
}
