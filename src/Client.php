<?php

declare(strict_types=1);

namespace Waffler\Rest;

use BadMethodCallException;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionException;
use Waffler\Definitions\Client as Contract;
use Waffler\Definitions\ResponseTransformer;
use Waffler\Rest\Lib\Implementer;
use Waffler\Rest\Lib\InternalResponseTransformer;
use Waffler\Rest\Lib\Method;

/**
 * Class Client
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Rest
 * @template T of object
 */
class Client implements Contract
{
    /**
     * @var \Psr\Http\Message\ResponseInterface|null
     */
    public ?ResponseInterface $lastResponse = null;

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected ClientInterface $client;

    /**
     * @var null|\ReflectionClass<T> $interface
     */
    protected ?ReflectionClass $interface = null;

    /**
     * @var array<int,ResponseTransformer> $responseTransformers
     */
    protected static array $responseTransformers = [];

    /**
     * Client constructor.
     *
     * @param class-string<T>     $interfaceClass
     * @param array<string,mixed> $guzzleClientConfig
     *
     * @throws \InvalidArgumentException
     */
    protected function __construct(string $interfaceClass, array $guzzleClientConfig = [])
    {
        $this->ensureValidInterface($interfaceClass);
        $this->client = new GuzzleClient($guzzleClientConfig);
    }

    /**
     * Here's where the generated client calls are
     * handled and dispatched to guzzle's client.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     * @throws \ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __call(string $name, array $arguments): mixed
    {
        if (!$this->interface) {
            throw new BadMethodCallException("The underlying client is not initialized.");
        }

        if (!$this->interface->hasMethod($name)) {
            throw new BadMethodCallException("The method $name is not declared in {$this->interface->getName()}.");
        }

        return $this->callClientMethod($name, $arguments);
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * @inheritDoc
     */
    public static function createFromInterface(string $interfaceName, string $baseUri = '', array $options = []): object
    {
        trigger_error('This method is deprecated, please upgrade to Client::implements()', E_USER_NOTICE);
        return self::implements($interfaceName, ['base_uri' => $baseUri] + $options);
    }

    /**
     * @inheritDoc
     */
    public static function addResponseTransformer(ResponseTransformer ...$responseTransformer): void
    {
        foreach ($responseTransformer as $transformer) {
            self::$responseTransformers[] = $transformer;
        }
    }

    /**
     * Register a callback for transforming an array to an \Traversable class.
     *
     * @param class-string<IT> $type
     * @param \Closure         $handler    The transformer receives the
     *                                     data array as first argument.
     *
     * @template IT of \Traversable
     */
    public static function registerAutoMappedListDataHandler(string $type, \Closure $handler): void
    {
        InternalResponseTransformer::registerDataHandler($type, $handler);
    }

    /**
     * Retrieve the list of response transformers for this client.
     *
     * @return array<int,\Waffler\Definitions\ResponseTransformer>
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function getResponseTransformers(): array
    {
        return [
            new InternalResponseTransformer(),
            ...self::$responseTransformers
        ];
    }

    public static function implements(string $interface, array $options = []): object
    {
        return (new self($interface, $options))->generate();
    }

    /**
     * @param class-string<T> $interfaceClass
     *
     * @throws \InvalidArgumentException
     */
    protected function ensureValidInterface(string $interfaceClass): void
    {
        try {
            $this->interface = new ReflectionClass($interfaceClass);
            if (!$this->interface->isInterface()) {
                $this->throwInvalidInterfaceException($interfaceClass);
            }
        } catch (ReflectionException) {
            $this->throwInvalidInterfaceException($interfaceClass);
        }
    }

    protected function throwInvalidInterfaceException(string $interface): void
    {
        throw new InvalidArgumentException("The value \"$interface\" is not a valid fully qualified interface name.");
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     * @throws \ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function callClientMethod(string $name, array $arguments): mixed
    {
        return $this->newMethod($name, $arguments)->call();
    }

    /**
     * Generates the interface implementation at runtime.
     *
     * @return T
     * @throws \Exception
     */
    protected function generate(): object
    {
        return $this->newImplementer()->make($this);
    }

    /**
     * Retrieves new implementer instance.
     *
     * @return \Waffler\Rest\Lib\Implementer<T,\Waffler\Rest\Client>
     * @throws \Exception
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     * @psalm-suppress MoreSpecificReturnType, LessSpecificReturnStatement, PossiblyNullArgument
     */
    private function newImplementer(): Implementer
    {
        return new Implementer($this->interface);
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return \Waffler\Rest\Lib\Method<T>
     * @throws \ReflectionException
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     * @psalm-suppress PossiblyNullReference
     */
    private function newMethod(string $name, array $arguments): Method
    {
        return new Method(
            $this->interface->getMethod($name),
            $arguments,
            $this,
        );
    }
}
