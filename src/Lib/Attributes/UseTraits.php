<?php

namespace Waffler\Rest\Lib\Attributes;

/**
 * Attribute UseTraits.
 *
 * You must use this attribute in your client interface if you want to
 * use any trait in the generated implementation.
 *
 * Example:
 * <pre>
 * #[UseTraits([FooTrait::class])]
 * interface Foo {}
 * </pre>
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Rest\Lib\Attributes
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class UseTraits
{
    /**
     * @param array<class-string> $traits
     */
    public function __construct(
        public array $traits
    ) {
    }
}
