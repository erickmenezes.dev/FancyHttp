<?php

declare(strict_types=1);

namespace Waffler\Rest\Lib;

use BadMethodCallException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionMethod;
use Waffler\Definitions\Attributes\AbstractHttpMethod;
use Waffler\Definitions\Attributes\Path;
use Waffler\Definitions\Attributes\RawOptions;
use Waffler\Definitions\Attributes\Suppress;
use Waffler\Definitions\Attributes\Unwrap;
use Waffler\Definitions\MethodInterface;
use Waffler\Rest\Client;
use Waffler\Rest\Traits\InteractsWithAutoMappedTypes;

/**
 * Class Method
 *
 * @author         ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package        Waffler\Rest\Lib
 * @internal
 * @psalm-suppress PropertyNotSetInConstructor
 * @template T of object
 */
class Method implements MethodInterface
{
    use InteractsWithAutoMappedTypes;

    protected AbstractHttpMethod $verb;

    protected string $returnType;

    protected Parameters $parameters;

    /**
     * @param \ReflectionMethod    $method
     * @param array                $arguments
     * @param \Waffler\Rest\Client<T> $parent
     * @psalm-suppress UndefinedMethod
     */
    public function __construct(protected ReflectionMethod $method, array $arguments, protected Client $parent)
    {
        $this->parameters = new Parameters($method->getParameters(), $arguments);
        $this->returnType = $this->method->getReturnType()?->getName() ?? 'mixed';
        $this->loadVerb();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function call(): mixed
    {
        $response = $this->parent->getClient()->requestAsync(
            $this->verb::method(),
            $this->parameters->parsePath($this->getPath()),
            $this->getOptions()
        );

        if ($this->isAsynchronous()) {
            return $response;
        }

        try {
            return $this->transformResponse($this->parent->lastResponse = $response->wait());
        } catch (BadResponseException $badResponseException) {
            $this->parent->lastResponse = $badResponseException->getResponse();
            throw $badResponseException;
        }
    }

    /**
     * @return string|class-string
     */
    public function getReturnType(): string
    {
        return $this->returnType;
    }

    public function isSuppressed(): bool
    {
        return $this->doesItHasAttribute($this->method, Suppress::class);
    }

    public function returnsAutoMapped(): false|ReflectionClass
    {
        return $this->isAutoMapped($this->returnType);
    }

    public function mustUnwrap(): bool
    {
        return $this->doesItHasAttribute($this->method, Unwrap::class);
    }

    public function getWrapperProperty(): string
    {
        return $this->getAttributeInstance($this->method, Unwrap::class)->property;
    }

    public function isAsynchronous(): bool
    {
        return is_a($this->returnType, PromiseInterface::class, true);
    }

    public function returnsAutoMappedList(): false|ReflectionClass
    {
        return $this->doesItReturnsAutoMappedList($this->method);
    }

    public function hasAttribute(string $name): false|array
    {
        return $this->doesItHasAttribute($this->method, $name)
            ? $this->getAttributeInstances($this->method, $name)
            : false;
    }

    public function getPath(): string
    {
        $path = [];

        if ($this->doesItHasAttribute($this->method->getDeclaringClass(), Path::class)) {
            $piece = $this->getAttributeInstance($this->method->getDeclaringClass(), Path::class)->path;
            $this->addPathParts($piece, $path);
        }

        if ($this->hasAttribute(Path::class)) {
            $piece = $this->getAttributeInstance($this->method, Path::class)->path;
            $this->addPathParts($piece, $path);
        }

        $this->addPathParts($this->verb->path, $path);

        return join('/', $path);
    }

    // protected

    protected function addPathParts(string $path, array &$parts): void
    {
        foreach (explode('/', $path) as $item) {
            if (empty($item)) {
                continue;
            }
            $parts[] = $item;
        }
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed
     * @throws \TypeError|\Exception If the response type is not recognized.
     */
    protected function transformResponse(ResponseInterface $response): mixed
    {
        foreach ($this->parent->getResponseTransformers() as $transformer) {
            try {
                return $transformer->handle($response, $this);
            } catch (\TypeError) {
            }
        }

        throw new \TypeError("It was not possible to cast the return type to \"{$this->getReturnType()}\"");
    }

    /**
     * @return array<string, mixed>
     * @throws \Exception
     */
    protected function getOptions(): array
    {
        $options = array_filter([
            RequestOptions::HEADERS => $this->parameters->getHeaderParams(),
            RequestOptions::BODY => $this->parameters->getBodyParam(),
            RequestOptions::JSON => $this->parameters->getJsonParams(),
            RequestOptions::QUERY => $this->parameters->getQueryParams(),
            RequestOptions::FORM_PARAMS => $this->parameters->getFormParams(),
            RequestOptions::MULTIPART => $this->parameters->getMultipartParams(),
            RequestOptions::AUTH => $this->parameters->getAuthParams()
        ]);

        $options[RequestOptions::HTTP_ERRORS] = !$this->isSuppressed();

        return array_merge($options, $this->parameters->getRawOptions());
    }

    protected function loadVerb(): void
    {
        foreach ($this->method->getAttributes() as $attribute) {
            $instance = $attribute->newInstance();
            if (is_a($instance, AbstractHttpMethod::class)) {
                $this->verb = $instance;
                return;
            }
        }
        throw new BadMethodCallException("The method {$this->method->getName()} has no verb attribute.");
    }
}
